# Any copyright is dedicated to the Public Domain:
#     <http://creativecommons.org/publicdomain/zero/1.0/>
#
# Written by Alfonso Sabato Siciliano <https://alfonsosiciliano.gitlab.io>

OUTPUT=  mixertui
SOURCES= mixertui.c
OBJECTS= ${SOURCES:.c=.o}

# BASE ncurses
CFLAGS= -Wall -Wextra -I/usr/local/include
LDFLAGS= -L/usr/lib -lncursesw -ltinfow \
	-L/usr/local/lib -lsysctlmibinfo2 -lbsddialog

RM= rm -f

all : ${OUTPUT}

clean:
	${RM} ${OUTPUT} *.o *~ *.core ${MAN}.gz

${OUTPUT}: ${OBJECTS}
	${CC} ${LDFLAGS} ${OBJECTS} -o ${.PREFIX}

.c.o:
	${CC} ${CFLAGS} -c ${.IMPSRC} -o ${.TARGET}

