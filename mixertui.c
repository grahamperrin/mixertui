/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2020-2023 Alfonso Sabato Siciliano
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/param.h>
#include <sys/ioctl.h>
#include <sys/sysctl.h>
#include <sys/soundcard.h>

#include <bsddialog.h>
#include <bsddialog_theme.h>
#include <curses.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sysctlmibinfo2.h>
#include <time.h>
#include <unistd.h>

#define VERSION 	"1.5-devel"

#define LINES_HEADER	7
#define COLS_HEADER	45
#define HIDE_HEADER	(COLS < COLS_HEADER || LINES < LINES_HEADER + 2)

#define GET_COLOR(bg, fg) (COLOR_PAIR(bg * 8 + fg + 1))
/* Foreground on BLACK background */
#define WHITE   GET_COLOR(COLOR_WHITE,  COLOR_BLACK)
#define YELLOW  GET_COLOR(COLOR_YELLOW, COLOR_BLACK)
#define CYAN    GET_COLOR(COLOR_CYAN,   COLOR_BLACK)
#define BLACK   GET_COLOR(COLOR_BLACK,  COLOR_BLACK)

static struct bsddialog_conf conf;
static struct bsddialog_theme theme;

enum viewmode { PLAYBACK, CAPTURE, ALL, NODEVICE };

#define ITEM_NAME	10
struct item {
	int id;
	unsigned int abspos;
	unsigned int relpos;
#define POSITION(view, item) (view == ALL ? item->abspos : item->relpos)
	char name[ITEM_NAME + 1];
	bool lmute;
	unsigned int lvolume;
	bool rmute;
	unsigned int rvolume;
	bool is_playback;
};

#define MAXDEVICES	100
#define DEV_NAME	6
#define DEV_DESC	50
struct device {
	int fd;
	int unit;
	char name[DEV_NAME];
	char desc[DEV_DESC];
	bool is_default;
	unsigned int nitems;
	unsigned int nplaybacks;
	unsigned int ncaptures;
	struct item items[SOUND_MIXER_NRDEVICES];
	int curr_item;
};

void usage(void);
/* View Layer */
int  init_tui(bool enable_color);
void set_popup_theme(int colorpair);
void draw_text(const char* text, int y, int x, bool bold, int colorpair);
void draw_title_and_borders(void);
void draw_header(enum viewmode view, struct device *dev);
int  get_prev_item(enum viewmode view, struct device *dev, int start_item);
int  get_next_item(enum viewmode view, struct device *dev, int start_item);
#define GET_FIRST_ITEM(view, dev) get_next_item(view, dev, -1)
void draw_item(enum viewmode view, int first, struct item *item, int last,
    bool is_selected);
void draw_items(enum viewmode view, struct device *dev, int *first, int *last);
/* Popup */
void error_popup(const char *error, bool show_errno);
void sysctlinfo_error_popup(void);
void help_popup(void);
int  select_device_popup(char *mixer);
void sysctl_popup(const char *title, const char *root);
/* Model/Hardware Layer */
int  get_default_device_index(void);
bool set_default_device(struct device *dev);
bool get_device(int dev_unit, struct device *dev);
bool set_volume(struct device *dev);
int  get_ndevices(char *mixer);
void save_profile(char *mixer);
int  load_profile(char *filename);

void usage(void)
{
	printf("usage: mixertui -h | -v\n");
	printf("       mixertui -P <profile>\n");
	printf("       mixertui [-c] [-d <unit>] [-m <mixer>] [-p <profile>] "\
	    "[-V p|c|a]\n");
}

/* The main() is the Controller Layer */
int main(int argc, char *argv[argc])
{
	int input, dev_unit, first, last;
	bool loop = true, enable_color = true;
	char mixer[MAXPATHLEN] = "/dev/mixer";
	char *profilefile = NULL;
	char sysctl_pcm[15];
	struct device dev;
	enum viewmode view = PLAYBACK;

	dev_unit = get_default_device_index();
	while ((input = getopt(argc, argv, "cd:hm:P:p:V:v")) != -1) {
		switch (input) {
		case 'c':
			enable_color = false;
			break;
		case 'd':
			dev_unit = atoi(optarg);
			break;
		case 'h':
			usage();
			printf("\n");
			printf(" -c\t\t Disable color\n");
			printf(" -d <unit>\t Open device with <unit> number\n");
			printf(" -h\t\t Display this help\n");
			printf(" -m <mixer>\t Use <mixer> to get devices\n");
			printf(" -P <profile>\t Set <profile> and quit (EXPERIMENTAL)\n");
			printf(" -p <profile>\t Load and set <profile> (EXPERIMENTAL)\n");
			printf(" -V p|c|a\t Display mode playback|capture|all\n");
			printf(" -v\t\t Print version and quit\n");
			printf("\n");
			printf("Press F1 inside MixerTUI for runtime features.\n");
			printf("See \'man mixertui\' for more information.\n");
			return 0;
		case 'm':
			strcpy(mixer, optarg);
			break;
		case 'P':
			return load_profile(optarg);
		case 'p':
			profilefile = optarg;
			break;
		case 'V':
			if(strcmp(optarg, "playback") == 0 || strcmp(optarg, "p") == 0)
				view = PLAYBACK;
			else if(strcmp(optarg, "capture") == 0 || strcmp(optarg, "c") == 0)
				view = CAPTURE;
			else if(strcmp(optarg, "all") == 0 || strcmp(optarg, "a") == 0)
				view = ALL;
			else {
				usage();
				return 1;
			}
			break;
		case 'v':
			printf("MixerTUI %s\n", VERSION);
			return 0;
		default:
			usage();
			return 1;
		}
	}
	argc -= optind;
	argv += optind;

	if(init_tui(enable_color) != 0) {
		printf("Cannot init curses TUI\n");
		return 1;
	}
	draw_title_and_borders();
	set_popup_theme(GET_COLOR(COLOR_WHITE, COLOR_BLUE));

	if(profilefile != NULL) {
		load_profile(profilefile);
		dev_unit = get_default_device_index();
	}

	if(get_device(dev_unit, &dev))
		dev.curr_item = GET_FIRST_ITEM(view, &dev);
	else
		view = NODEVICE;

	draw_header(view, &dev);
	first = last = 0;
	draw_items(view, &dev, &first, &last);

	while(loop) {
		input = getch();
		switch(input) {
		case 'Q':
		case 'q':
		case  27: /* Esc */
			loop = false;
			break;
		case KEY_F(1):
		case '?':
		case 'H':
		case 'h':
			help_popup();
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(2):
			sysctl_popup(" Sound Driver ", "hw.snd");
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case '\t': /* TAB */
			if(view == NODEVICE)
				break;
			if(view == PLAYBACK) {
				view = CAPTURE;
				dev.curr_item = GET_FIRST_ITEM(view, &dev);
			} else if(view == CAPTURE) {
				view = ALL;
				if(dev.curr_item < 0)
					dev.curr_item = GET_FIRST_ITEM(view, &dev);
			} else { /* view == ALL */
				view = PLAYBACK;
				if(dev.curr_item >= 0 && 
				    !dev.items[dev.curr_item].is_playback)
					dev.curr_item = GET_FIRST_ITEM(view, &dev);
			}
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(3):
			if(view == NODEVICE || view == PLAYBACK)
				break;
			view = PLAYBACK;
			if(dev.curr_item < 0 || !dev.items[dev.curr_item].is_playback)
				dev.curr_item = GET_FIRST_ITEM(view, &dev);
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(4):
			if(view == NODEVICE || view == CAPTURE)
				break;
			view = CAPTURE;
			if(dev.curr_item < 0 || dev.items[dev.curr_item].is_playback)
				dev.curr_item = GET_FIRST_ITEM(view, &dev);
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(5):
			if(view == NODEVICE || view == ALL)
				break;
			view = ALL;
			if(dev.curr_item < 0)
				dev.curr_item = GET_FIRST_ITEM(view, &dev);
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(6):
			if((dev_unit = select_device_popup(mixer)) >= 0) {
				if(view != NODEVICE && dev_unit == dev.unit) {
					draw_header(view, &dev);
					draw_items(view, &dev, &first, &last);
					break;
				}
				if(view != NODEVICE)
					close(dev.fd);
				if(get_device(dev_unit, &dev)) {
					first = last = 0;
					view = (view == NODEVICE) ? PLAYBACK : view;
					dev.curr_item = GET_FIRST_ITEM(view, &dev);
				} else {
					view = NODEVICE;
				}
			}
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(7):
			if(view == NODEVICE)
				break;
			sprintf(sysctl_pcm, "dev.pcm.%d", dev.unit);
			sysctl_popup(" Device info ", sysctl_pcm);
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_F(8):
			if(view == NODEVICE)
				break;
			if(!set_default_device(&dev))
				draw_items(view, &dev, &first, &last);
			draw_header(view, &dev);
			break;
		case 'b':
		case 'B':
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			dev.items[dev.curr_item].lvolume =
			    (dev.items[dev.curr_item].lvolume +
			     dev.items[dev.curr_item].rvolume) / 2;
			dev.items[dev.curr_item].rvolume =
			    dev.items[dev.curr_item].lvolume;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case 'M':
		case 'm':
		case '<':
		case '>':
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			if(input != '>')
				dev.items[dev.curr_item].lmute =
					!dev.items[dev.curr_item].lmute;
			if(input != '<')
				dev.items[dev.curr_item].rmute =
				    !dev.items[dev.curr_item].rmute;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case '9':
		case '8':
		case '7':
		case '6':
		case '5':
		case '4':
		case '3':
		case '2':
		case '1':
		case '0':
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			dev.items[dev.curr_item].lvolume = (input - 48) * 10;
			dev.items[dev.curr_item].rvolume = (input - 48) * 10;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case KEY_END:
		case KEY_HOME:
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			dev.items[dev.curr_item].lvolume = (input == KEY_END) ? 0 : 100;
			dev.items[dev.curr_item].rvolume = (input == KEY_END) ? 0 : 100;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case KEY_UP:
		case '+':
		case 's':
		case 'a':
		case 'd':
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			if(dev.items[dev.curr_item].lvolume < 100 && input != 'd')
				dev.items[dev.curr_item].lvolume++;
			if(dev.items[dev.curr_item].rvolume < 100 && input != 'a')
				dev.items[dev.curr_item].rvolume++;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case KEY_DOWN:
		case '-':
		case 'x':
		case 'z':
		case 'c':
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			if(dev.items[dev.curr_item].lvolume > 0 && input != 'c')
				dev.items[dev.curr_item].lvolume--;
			if(dev.items[dev.curr_item].rvolume > 0 && input != 'z')
				dev.items[dev.curr_item].rvolume--;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case KEY_NPAGE:
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			if(dev.items[dev.curr_item].lvolume < 10)
				dev.items[dev.curr_item].lvolume = 0;
			else
				dev.items[dev.curr_item].lvolume -= 10;
			if(dev.items[dev.curr_item].rvolume < 10)
				dev.items[dev.curr_item].rvolume = 0;
			else
				dev.items[dev.curr_item].rvolume -= 10;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case KEY_PPAGE:
			if(view == NODEVICE || dev.curr_item < 0)
				break;
			dev.items[dev.curr_item].lvolume += 10;
			if(dev.items[dev.curr_item].lvolume > 100)
				dev.items[dev.curr_item].lvolume = 100;
			dev.items[dev.curr_item].rvolume += 10;
			if(dev.items[dev.curr_item].rvolume > 100)
				dev.items[dev.curr_item].rvolume = 100;
			loop = set_volume(&dev);
			draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case KEY_LEFT:
			if(view == NODEVICE || dev.curr_item <= 0 ||
			    get_prev_item(view, &dev, dev.curr_item) < 0)
				break;
			draw_item(view, first, &dev.items[dev.curr_item], last, false);
			dev.curr_item = get_prev_item(view, &dev, dev.curr_item);
			draw_header(view, &dev);
			if((int)POSITION(view, (&dev.items[dev.curr_item])) < first)
				draw_items(view, &dev, &first, &last);
			else
				draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
			break;
		case KEY_RIGHT:
			if(view == NODEVICE || dev.curr_item < 0 ||
			    get_next_item(view, &dev, dev.curr_item) < 0)
				break;
			draw_item(view, first, &dev.items[dev.curr_item], last, false);
			dev.curr_item = get_next_item(view, &dev, dev.curr_item);
			draw_header(view, &dev);
			if((int)POSITION(view, (&dev.items[dev.curr_item])) >= last)
				draw_items(view, &dev, &first, &last);
			else
				draw_item(view, first, &dev.items[dev.curr_item], last, true);
			break;
		case 'p':
		case 'P':
			if(view == NODEVICE)
				break;
			save_profile(mixer);
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
			break;
		case KEY_RESIZE:
		case 'r':
		case 'R':
			draw_header(view, &dev);
			draw_items(view, &dev, &first, &last);
		}
	}

	endwin();
	return 0;
}

/* View */
void set_popup_theme(int colorpair)
{
	bsddialog_initconf(&conf);
	conf.shadow = false;
	conf.auto_topmargin = 2;
	conf.auto_downmargin = 1;
	conf.key.enable_esc = true;

	theme.screen.color = BLACK;

	theme.dialog.delimtitle = false;
	theme.dialog.color = colorpair;
	theme.dialog.titlecolor = theme.dialog.color;
	theme.dialog.lineraisecolor = theme.dialog.color;
	theme.dialog.linelowercolor = theme.dialog.color;
	theme.dialog.arrowcolor = theme.dialog.color;

	theme.button.f_color = colorpair | A_REVERSE;
	theme.button.f_shortcutcolor = theme.button.f_color;
	
	theme.menu.f_namecolor = colorpair | A_REVERSE;
	theme.menu.namecolor = theme.dialog.color;
	theme.menu.f_desccolor = theme.menu.f_namecolor;
	theme.menu.desccolor = theme.dialog.color;
	theme.menu.namesepcolor = theme.dialog.color;
	theme.menu.descsepcolor = theme.dialog.color;
	theme.menu.f_shortcutcolor = theme.menu.f_namecolor;
	theme.menu.shortcutcolor = theme.dialog.color;

	theme.button.minmargin = 1;
	theme.button.maxmargin = 5;
	theme.button.leftdelim = '[';
	theme.button.rightdelim = ']';

	bsddialog_set_theme(&theme);
}

int init_tui(bool enable_color)
{
	int c, i, j, error;

	if(initscr() == NULL)
		return -1;

	error = 0;
	error += keypad(stdscr, TRUE);
	nl();
	error += cbreak();
	error += noecho();
	curs_set(0);

	if (enable_color) {
		error += start_color();
		c = 1;
		error += start_color();
		for (i = 0; i < 8; i++) {
			for (j = 0; j < 8; j++) {
				error += init_pair(c, i, j);
				c++;
			}
		}
	}
	
	return error;
}

void draw_text(const char* text, int y, int x, bool bold, int colorpair)
{
	attron(colorpair | (bold ? A_BOLD : 0));
	mvaddstr(y, x, text);
	attroff(colorpair | (bold ? A_BOLD : 0));
}

void draw_title_and_borders(void)
{
	const char *title = "  MixerTUI  ";

	attron(CYAN);
	box(stdscr, 0, 0);
	attroff(CYAN);

	draw_text(title, 0, (COLS/2) - (strlen(title)/2), true, YELLOW);
}

void draw_header(enum viewmode view, struct device *dev)
{
	int i, j;
	char left[5][43] = {
		"Device",
		"Chip",
		"Status",
		"View   F3: Playback  F4: Capture  F5: All ",
		"Item"
	};
	char right[5][25] = {
		"F1:  Help          ",
		"F2:  Audio info    ",
		"F6:  Select device ",
		"F7:  Device info   ",
		"Esc: Exit          " 
	};

	if(HIDE_HEADER)
		return;

	for(i=1; i < LINES_HEADER; i++)
		for(j=1; j < COLS-1; j++)
			draw_text(" ", i, j, true, BLACK);

	for(i=0; i < 5; i++)
		draw_text(left[i], i+1, 2, true, CYAN);

	if (view != NODEVICE) {
		draw_text(dev->name, 1, 9, true, YELLOW);
		draw_text(dev->desc, 2, 9, true, YELLOW);
		if(dev->is_default)		
			draw_text("Default", 3, 9, true, YELLOW);
		else
			draw_text("F8: Set as default", 3, 9, true, CYAN);
		
		if (view == PLAYBACK)
			draw_text("[Playback]", 4, 12, true, YELLOW);
		else if (view == CAPTURE)
			draw_text("[Capture]", 4, 26, true, YELLOW);
		else if (view == ALL)
			draw_text("[All]", 4, 39, true, YELLOW);

		if(dev->curr_item >= 0)
			draw_text(dev->items[dev->curr_item].name, 5, 9, true, YELLOW);
	}

	/* right side*/
	if((int)(strlen(right[0]) + strlen(left[3]) + 3) > COLS)
		return;
	for(i=0; i < 5; i++)
		draw_text(right[i], i+1, COLS - strlen(right[i]) -1, true, CYAN);
}

int get_prev_item(enum viewmode view, struct device *dev, int start_item)
{
	int i;
	
	for(i=start_item-1; i>= 0; i--) {
		if(view == PLAYBACK && dev->items[i].is_playback)
			return i;
		if(view == CAPTURE && (dev->items[i].is_playback == false))
			return i;
		if(view == ALL)
			return i;
	}

	return -1;
}

int get_next_item(enum viewmode view, struct device *dev, int start_item)
{
	unsigned int i;
	
	for(i=start_item+1; i< dev->nitems; i++) {
		if(view == PLAYBACK && dev->items[i].is_playback)
			return i;
		if(view == CAPTURE && (dev->items[i].is_playback == false))
			return i;
		if(view == ALL)
			return i;
	}

	return -1;
}

void draw_item(enum viewmode view, int first, struct item *item, int last,
	bool is_selected)
{
	int y, x, i, pos, top, barH, barW, color_bar;
	WINDOW *bar;

	if(LINES < 4)
		return; /* responsive no error */

	pos = POSITION(view, item);
	y = LINES - 2;
	x = ((COLS -3) - ((ITEM_NAME +1) * (last - first))) / 2;
	x = x < 0 ? 0 : x;
	pos = pos - first;
	x += pos * (ITEM_NAME +1) + 1;

	if((x + ITEM_NAME + 1) >= (COLS -1))
		return; /* responsive no error */

	/* label */
	attron(is_selected ? (YELLOW | A_BOLD) : CYAN);
	mvprintw(y-1, x + 2, "%3d  %-3d", item->lvolume, item->rvolume);
	if(item->lmute)
		mvprintw(y-1, x + 2, "  M");
	if(item->rmute)
		mvprintw(y-1, x + 7, "M  ");
	attroff(is_selected ? (YELLOW | A_BOLD) : CYAN);
	draw_text("<>", y-1, x + 5, true, is_selected ? CYAN : WHITE);

	for(i=0; i<ITEM_NAME; i++)
		draw_text(" ", y, x + 1 + i, true, GET_COLOR(COLOR_CYAN, COLOR_CYAN));

	if(is_selected) {
		draw_text("<", y, x, true, YELLOW);
		draw_text(item->name, y,
				x + 1 + ((ITEM_NAME - strlen(item->name))/2),
				true, GET_COLOR(COLOR_YELLOW, COLOR_CYAN));
		draw_text(">", y, x + 1 + ITEM_NAME, true, YELLOW);
	} else {
		draw_text(" ", y, x, false, BLACK);
		draw_text(item->name, y,
				x + 1 + ((ITEM_NAME - strlen(item->name))/2),
				false, GET_COLOR(COLOR_BLACK, COLOR_CYAN));
		draw_text(" ", y, x + 1 + ITEM_NAME, false, BLACK);
	}

	/* bar */
	top = (COLS < COLS_HEADER) ? 2 : LINES_HEADER;
	if((y - top - 1 ) < 5)
		return; /* responsive no error */
	bar = newwin(y - top - 1, 4, top, x + (ITEM_NAME/2) - 1);
	wbkgd(bar, BLACK);

	wattron(bar, is_selected ? (YELLOW | A_BOLD) : CYAN);
	box(bar, 0, 0);
	wattroff(bar, is_selected ? (YELLOW | A_BOLD) : CYAN);

	color_bar = GET_COLOR(COLOR_GREEN, COLOR_GREEN);
	getmaxyx(bar, barH, barW);
	for(i=1; i < barH-1; i++) {
		if (i > (barH-2)/2)
			color_bar = GET_COLOR(COLOR_WHITE, COLOR_WHITE);
		if (i > (barH-2)*0.8)
			color_bar = GET_COLOR(COLOR_RED, COLOR_RED);
		wattron(bar, color_bar);
		wmove(bar, barH-1-i, 1);
		if(item->lvolume > i * (100/(float)barH))
			waddch(bar, '*');
		wmove(bar, barH-1-i, 2);
		if(item->rvolume > i * (100/(float)barH))
			waddch(bar, '*');
		wattroff(bar, color_bar);
	}

	wrefresh(bar);
	delwin(bar);
}

void draw_items(enum viewmode view, struct device *dev, int *first, int *last)
{
	int to_show, nitems, selected, pos;
	unsigned int i;

	move(HIDE_HEADER ? 1 : LINES_HEADER, 0);
	clrtobot();

	draw_title_and_borders();
	if(view == NODEVICE) {
		refresh();
		return;
	}

	if(view == ALL) {
		nitems = dev->nitems;
		selected = dev->curr_item;
	} else {
		nitems = (view == PLAYBACK) ? dev->nplaybacks : dev->ncaptures;
		selected = dev->curr_item < 0 ? -1 : dev->items[dev->curr_item].relpos;
	}
	
	to_show = (COLS - 3) / (ITEM_NAME + 1);
	to_show = nitems < to_show ? nitems : to_show;

	if(nitems <= to_show) {
		*first = 0;
	} else {
		if(selected < *first)
			*first = selected;
		else if(selected > *first + to_show - 1)
			*first = selected - to_show + 1;
	}
	*last = *first + to_show;
	
	if(*first > 0)
		for(i=0; i<10; i++)
			draw_text("<", LINES/2 - 5 + i, 0, true, CYAN);
	if(nitems > *last)
		for(i=0; i<10; i++)
			draw_text(">", LINES/2 - 5 + i, COLS-1, true, CYAN);

	refresh();

	for(i=0; i < dev->nitems; i++) {
		if(view == CAPTURE && dev->items[i].is_playback)
			continue;
		if(view == PLAYBACK && !dev->items[i].is_playback)
			continue;
		pos = (view == ALL) ? i : dev->items[i].relpos;
		if(pos < *first || pos >= *last)
			continue;
		draw_item(view, *first, &dev->items[i], *last, false);
	}
	if(dev->curr_item >= 0)
		draw_item(view, *first, &dev->items[dev->curr_item], *last, true);
}

/* Popup */
void error_popup(const char *error, bool show_errno)
{
	char *text;

	set_popup_theme(GET_COLOR(COLOR_WHITE, COLOR_RED));

	asprintf(&text, "%s\n%s\n", error, show_errno ? strerror(errno) : "");
	conf.title = " Error ";
	conf.button.ok_label = "Exit";
	bsddialog_msgbox(&conf, text, 0, 0);
	free(text);

	set_popup_theme(GET_COLOR(COLOR_WHITE, COLOR_BLUE));
}

void sysctlinfo_error_popup(void)
{
	const char *msg="Make sure that you have loaded the\n" \
		  "sysctlinfo kernel module, by doing:\n\n" \
		  "     # kldload sysctlinfo\n\n" \
		  "or adding:\n\n" \
		  "     sysctlinfo_load=\"YES\"\n\n" \
		  "to your /boot/loader.conf";

	error_popup(msg, false);
}

void help_popup(void)
{
	const char *text = \
		"              Version " VERSION "\n" \
		"Esc q Q    Exit\n" \
	 	"F1 ? H h   Help\n" \
		"F2         Sound System information\n" \
	 	"F3         Show playback controls\n" \
	 	"F4         Show capture controls\n" \
	 	"F5         Show all controls\n" \
	 	"Tab        Switch view F3\\F4\\F5\n" \
	 	"F6         Select device\n" \
		"F7         Device information\n" \
		"F8         Set current device as default\n" \
		"P p        Save profile (EXPERIMENTAL)\n" \
		"R r        Refresh screen\n" \
		"Left       Select previous control\n" \
		"Right      Select next control\n" \
		"Up\\Down    Increment\\Decrement volume\n" \
		"+\\-        Increment\\Decrement volume\n" \
		"Page UP    Increment volume in a big step\n" \
		"Page DOWN  Decrement volume in a big step\n" \
		"End        Set volume to 0%\n" \
		"Home       Set volume to 100%\n" \
		"0-9        Set volume to 0%-90%\n" \
		"a\\s\\d      Increment left\\both\\right volume\n" \
		"z\\x\\c      Decrement left\\both\\right volume\n" \
		"B b        Balance left and right volume\n" \
		"M m        Toggle mute\n" \
		"<\\>        Toggle left\\right mute\n";
	
	conf.title = " HELP ";
	conf.button.ok_label = "Exit";
	bsddialog_msgbox(&conf, text, 0, 0);
}

int select_device_popup(char *mixer)
{
	int i, ndevices, selected, button;
	char oidname[DEV_NAME + 12];
	char pcm[MAXDEVICES][DEV_NAME];
	char desc[MAXDEVICES][DEV_DESC];
	size_t desclen;
	struct bsddialog_menuitem item[MAXDEVICES];

	if((ndevices = get_ndevices(mixer)) < 0)
		return - 1;

	for(i=0; i < ndevices; i++){
		item[i].prefix = "";
		item[i].on = false;
		item[i].depth = 0;
		memset(&pcm[i], 0, DEV_NAME);
		snprintf(pcm[i], DEV_NAME, "pcm%d", i);
		item[i].name = pcm[i];
		sprintf(oidname, "dev.pcm.%d.%%desc", i);
		desclen = DEV_DESC;
		memset(&desc[i], 0, DEV_DESC);
		sysctlbyname(oidname, desc[i], &desclen, NULL, 0);
		item[i].desc = desc[i];
		item[i].bottomdesc = "";
	}

	conf.title = " Select Device ";
	conf.button.ok_label = "Select";
	selected = -1;
	button = bsddialog_menu(&conf, "", 0, 0, 0, ndevices, item, &selected);

	return (button == BSDDIALOG_OK ? selected : -1);
}

void sysctl_popup(const char *title, const char *root)
{
	const char *pname;
	struct sysctlmif_object_list *list;
	struct sysctlmif_object *obj;
	size_t valuesize;
	char value[MAXPATHLEN];
	char *text, *current, *old;

	if((list = sysctlmif_grouplistbyname(root)) == NULL) {
		sysctlinfo_error_popup();
		return;
	}

	asprintf(&old, "");
	SLIST_FOREACH(obj, list, object_link) {
		valuesize = MAXPATHLEN;
		if(sysctl(obj->id, obj->idlevel, value, &valuesize, NULL, 0) != 0)
			continue;

		if(obj->type == CTLTYPE_STRING && strlen(value) == 0)
			continue;

		pname = &obj->name[strlen(root)+1];
		switch (obj->type) {
		case CTLTYPE_INT:
			asprintf(&current, "%s\n%s: %d\n\n",
			    obj->desc, pname, *((int*)value));
			break;
		case CTLTYPE_UINT:
			asprintf(&current, "%s\n%s: %u\n\n",
			    obj->desc, pname,  *((unsigned int*)value));
			break;
		case CTLTYPE_STRING:
			value[valuesize] = '\0';
			asprintf(&current, "%s\n%s: %s\n\n",
			    obj->desc, pname,  value);
		}
		asprintf(&text, "%s%s", old, current);
		free(old);
		old = text;
	}
	sysctlmif_freelist(list);

	conf.title = title;
	conf.button.ok_label = "EXIT";
	bsddialog_msgbox(&conf, text, 0, 55);
	free(text);
}

/* Model */
int get_ndevices(char *mixer)
{
	int fd;
	struct oss_sysinfo si;

	if ((fd = open(mixer, O_RDWR)) == -1) {
		error_popup(mixer, true);
		return -1;
	}

	if (ioctl(fd, SNDCTL_SYSINFO, &si) == -1) {
		error_popup("SNDCTL_SYSINFO", true);
		return -1;
	}

	close(fd);

	return si.nummixers;
}

int get_default_device_index(void)
{
	int unit;
	size_t size = sizeof(int);

	if(sysctlbyname("hw.snd.default_unit", &unit, &size, NULL, 0) !=0)
		return -1;

	return unit;
}

bool set_default_device(struct device *dev)
{
	size_t size = sizeof(int);
	char error[40];

	if (sysctlbyname("hw.snd.default_unit", NULL, 0, &dev->unit, size) < 0) {
		sprintf(error, "sysctl hw.snd.default_unit=%d", dev->unit);
		error_popup(error, true);
		return false;
	}

	dev->is_default = true;

	return true;
}

bool get_device(int dev_unit, struct device *dev)
{
	int i, fd, volume, devmask, recmask;
	char pcm[DEV_NAME], oidname[30], desc[DEV_DESC], mixer[30];
	size_t desclen = DEV_DESC;
	const char *names[SOUND_MIXER_NRDEVICES] = SOUND_DEVICE_NAMES;
	
	sprintf(mixer, "/dev/mixer%d", dev_unit);
	if ((fd = open(mixer, O_RDWR)) == -1) {
		error_popup(mixer, true);
		return false;
	}
	
	/*
	 * SNDCTL_DSP_GET_RECSRC_NAMES is implemented,
	 * SNDCTL_DSP_GET_PLAYTGT_NAMES is not fully implemented,
	 * so use the old interface
	 */
	if (ioctl(fd, SOUND_MIXER_READ_DEVMASK, &devmask) == -1) {
		error_popup("SOUND_MIXER_READ_DEVMASK", true);
		return false;
	}
	if (ioctl(fd, SOUND_MIXER_READ_RECMASK, &recmask) == -1) {
		error_popup("SOUND_MIXER_READ_RECMASK", true);
		return false;
	}

	dev->fd=fd;
	dev->unit = dev_unit;

	sprintf(pcm, "pcm%d", dev_unit);
	strcpy(dev->name, pcm);

	sprintf(oidname, "dev.pcm.%d.%%desc", dev_unit);
	sysctlbyname(oidname, desc, &desclen, NULL, 0);
	strncpy(dev->desc, desc, desclen);
	dev->desc[desclen] = '\0';

	dev->is_default = dev_unit == get_default_device_index() ? true : false;

	dev->nitems = 0;
	dev->nplaybacks = 0;
	dev->ncaptures = 0;
	for (i = 0; i < SOUND_MIXER_NRDEVICES; i++) {
		if (!((1 << i) & devmask))
			continue;
		dev->items[dev->nitems].id=i;
		strncpy(dev->items[dev->nitems].name, names[i], ITEM_NAME);
		dev->items[dev->nitems].name[ITEM_NAME] = '\0';

		if (ioctl(fd, MIXER_READ(i), &volume) == -1)
			volume = 0;
		dev->items[dev->nitems].lvolume = volume & 0x7f;
		dev->items[dev->nitems].rvolume = (volume >> 8) & 0x7f;
		dev->items[dev->nitems].lmute = false;
		dev->items[dev->nitems].rmute = false;

		if ((1 << i) & recmask) {
			dev->items[dev->nitems].is_playback = false;
			dev->items[dev->nitems].relpos = dev->ncaptures;
			dev->ncaptures++;
		} else {
			dev->items[dev->nitems].is_playback = true;
			dev->items[dev->nitems].relpos = dev->nplaybacks;
			dev->nplaybacks++;
		}

		dev->items[dev->nitems].abspos = dev->nitems;
		dev->nitems++;
	}

	return true;
}

bool set_volume(struct device *dev)
{
	int volume;
	struct item *item = &dev->items[dev->curr_item];

	/*
	 * SNDCTL_MIX_EXTINFO and MIXT_ONOFF are not implemented,
	 * mute is handled in-software
	 */
	volume = item->lmute ? 0 : item->lvolume;
	volume = volume | (item->rmute ? 0 : item->rvolume << 8);
	if (ioctl(dev->fd, MIXER_WRITE(item->id), &volume) == -1) {
		error_popup("Cannot change volume, RESTART.", true);
		return false;
	}

	return true;
}

void save_profile(char *mixer)
{
	char cwd[MAXPATHLEN], filename[MAXPATHLEN], value[BUFSIZ];
	char *text;
	size_t valuesize;
	FILE *fp;
	struct device dev;
	int i, ndevs;
	unsigned int j;
	time_t clock;
	struct sysctlmif_object_list *list;
	struct sysctlmif_object *obj;

	if(time(&clock) < 0) {
		error_popup("Cannot save the profile", true);
		return;
	}

	getcwd(cwd, MAXPATHLEN);
	snprintf(filename, MAXPATHLEN, "%s/mixertui-%ld.conf", cwd, clock);
	if ((fp = fopen(filename, "w")) == NULL) {
		error_popup("Cannot save the profile:", true);
		return;
	}

	fprintf(fp, "### MixerTUI profile - %s", ctime(&clock));
	fprintf(fp, "version %s\n", VERSION);

	ndevs = get_ndevices(mixer);
	for(i=0; i<ndevs; i++) {
		fputs("\n", fp);
		get_device(i, &dev);
		fprintf(fp, "## %s - %s\n", dev.name, dev.desc);
		fputs("# VOLUME PCM NAME ID LVOL LMUTE RVOL RMUTE\n" ,fp);
		for(j=0; j < dev.nitems; j++) {
			fprintf(fp, "volume %d %s %i %u %s %u %s\n",
			    dev.unit,
			    dev.items[j].name,
			    dev.items[j].id,
			    dev.items[j].lvolume,
			    dev.items[j].lmute ? "mute" : "unmute",
			    dev.items[j].rvolume,
			    dev.items[j].rmute ? "mute" : "unmute");
		}
	}

	fputs("\n## sysctl objects\n", fp);
	if((list = sysctlmif_grouplistbyname("hw.snd")) == NULL) {
		sysctlinfo_error_popup();
		return;
	}
	SLIST_CONCAT(list, sysctlmif_grouplistbyname("dev.pcm"),
			sysctlmif_object, object_link);

	SLIST_FOREACH(obj, list, object_link) {
		if(!(obj->flags & CTLFLAG_WR))
			continue;
		valuesize = BUFSIZ;
		if(sysctl(obj->id, obj->idlevel, value, &valuesize, NULL, 0) != 0)
			continue;
		if(obj->type == CTLTYPE_STRING && strlen(value) == 0)
			continue;

		if(!(obj->flags & CTLFLAG_ANYBODY))
			fprintf(fp, "# ");
		if(obj->type == CTLTYPE_INT)
			fprintf(fp, "sysctl %s %d\n", obj->name, *((int*)value));
		else if(obj->type == CTLTYPE_UINT)
			fprintf(fp, "sysctl %s %u\n",obj->name, *((unsigned int*)value));
		else if(obj->type == CTLTYPE_STRING)
			fprintf(fp, "sysctl %s %s\n", obj->name, value);
	}
	sysctlmif_freelist(list);

	fclose(fp);

	conf.title = " Info ";
	conf.button.ok_label = "Exit";
	asprintf(&text, "%s\n\n%s\n\n%s\n\n%s",
		"Current profile saved:",
		filename,
		"To load <profile> lunch MixerTUI with -p option:",
		"$> mixertui -p <profile>");
	bsddialog_msgbox(&conf, text, 0, 0);
	free(text);
}

int load_profile(char *filename)
{
	FILE *fp;
	int pcm, iditem, valueint;
	unsigned int i, lvol, rvol, newvaluesize, valueuint;
	char line[BUFSIZ], option[10], sysctlname[MAXPATHLEN], *parse;
	char nameitem[ITEM_NAME + 1], valuestr[BUFSIZ], lmute[7], rmute[7];
	void *newvalue;
	struct sysctlmif_object *obj;
	struct device dev;

	if((fp = fopen(filename, "r")) == NULL) {
		error_popup("Cannot load the profile:", true);
		return 1;
	}

	while(fgets(line, BUFSIZ, fp) != NULL) {
		if(line[0] == '#' || line[0] == '\n')
			continue;
		sscanf(line, "%s", option);
		parse = strchr(line, ' ') + 1;
		if(strcmp(option, "version") == 0) {
			; // nothing for now
		}
		else if(strcmp(option, "volume") == 0) {
			sscanf(parse, "%d %s %i %u %s %u %s",
			    &pcm, nameitem, &iditem, &lvol, lmute, &rvol, rmute);
			if(get_device(pcm, &dev) == false)
				return 1;
			for(i=0; i<dev.nitems; i++){
			    if(iditem == dev.items[i].id){
				dev.items[i].lmute = strcmp(lmute, "mute") == 0 ? 
					true : false;
				dev.items[i].lvolume = lvol;
				dev.items[i].rmute = strcmp(rmute, "mute") == 0 ?
					true : false;
				dev.items[i].rvolume = rvol;
				dev.curr_item = i;
				if(set_volume(&dev) == false)
					return 1;
				break;
			    }
			}
			close(dev.fd);
		}
		else if(strcmp(option, "sysctl") == 0) {
		    sscanf(parse, "%s %s", sysctlname, valuestr);
		    if((obj = sysctlmif_objectbyname(sysctlname)) == NULL) {
			sysctlinfo_error_popup();
			return 1;
		    }
		    if(obj->type == CTLTYPE_INT) {
			newvaluesize = sizeof(int);
			valueint = (int)strtoll(valuestr, NULL, 0); 
			newvalue = &valueint;
		    }
		    else if(obj->type == CTLTYPE_UINT) {
			newvaluesize = sizeof(unsigned int);
			valueuint = (u_int)strtoull(valuestr, NULL, 0);
			newvalue = &valueuint;
		    }
		    else if (obj->type == CTLTYPE_STRING) {
			newvaluesize = strlen(valuestr);
			newvalue = valuestr;
		    }
		    sysctl(obj->id, obj->idlevel, NULL, 0, newvalue, newvaluesize);
		    sysctlmif_freeobject(obj);
		}
	}

	fclose(fp);

	return 0;
}
