# mixertui 1.4.2

Audio Mixer with a Text User Interface for [FreeBSD](https://www.freebsd.org).

## Getting Started

To install the port [audio/mixertui](https://www.freshports.org/audio/mixertui):

	# cd /usr/ports/audio/mixertui/ && make install clean

To add the package:

	# pkg install mixertui


![screenshot](screenshot.png)  

## Documentation

Usage:

```
$> mixertui -h
usage: mixertui -h | -v
       mixertui -P <profile>
       mixertui [-c] [-d <unit>] [-m <mixer>] [-p <profile>] [-V p|c|a]

 -c		 Disable color
 -d <unit>	 Open device with <unit> number
 -h		 Display this help
 -m <mixer>	 Use <mixer> to get devices
 -P <profile>	 Set <profile> and quit (EXPERIMENTAL)
 -p <profile>	 Load and set <profile> (EXPERIMENTAL)
 -V p|c|a	 Display mode playback|capture|all
 -v		 Print version and quit

Press F1 inside MixerTUI for runtime features.
See 'man mixertui' for more information.
```

Manual: 

 - `$> man mixertui`
 - <https://man.freebsd.org/mixertui/8>

